import React from 'react';
import PropTypes from 'prop-types';
import deburr from 'lodash/deburr';
import Autosuggest from 'react-autosuggest';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles } from '@material-ui/core/styles';
import axios from 'axios';
import Typography from '@material-ui/core/Typography';


function renderInputComponent(inputProps) {
  const { classes, inputRef = () => {}, ref, ...other } = inputProps;

  return (
    <TextField
      fullWidth
      InputProps={{
        inputRef: node => {
          ref(node);
          inputRef(node);
        },
        classes: {
          input: classes.input,
        },
      }}
      {...other}
    />
  );
}

function renderSuggestion(suggestion, { query, isHighlighted }) {
  const matches = match(suggestion.symbol, query);
  const parts = parse(suggestion.symbol, matches);

  return (
    <MenuItem selected={isHighlighted} component="div">
      <div>
        {parts.map((part, index) => {
          return part.highlight ? (
            <span key={String(index)} style={{ fontWeight: 500 }}>
              {part.text}
            </span>
          ) : (
            <strong key={String(index)} style={{ fontWeight: 300 }}>
              {part.text}
            </strong>
          );
        })}
        <span>{'  - '}{suggestion.name }</span>
      </div>
    </MenuItem>
  );
}

const  getSuggestions =  (value, data) => {
  const inputValue = deburr(value.trim()).toLowerCase();
  const inputLength = inputValue.length;
  let count = 0;
  return inputLength === 0
    ? []
    : data.filter(suggestion => {
        const keep =
          count < 5 && suggestion.symbol.slice(0, inputLength).toLowerCase() === inputValue;

        if (keep) {
          count += 1;
        }

        return keep;
      });
}

function getSuggestionValue(suggestion) {
  return suggestion.symbol ;
}

const styles = theme => ({
  auto: {
    width:500,
    margin:'auto'
  },
  head : {
    marginBottom: theme.spacing.unit *20
  },
  root: {
 background: '#6ae0d5',
  textAlign: 'center',
  paddingTop: 20,
  flexGrow: 1,
  height:'100vh'
  },
  container: {
    position: 'relative',
  },
  suggestionsContainerOpen: {
    position: 'absolute',
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
  },
  suggestion: {
    display: 'block',
  },
  suggestionsList: {
    margin: 0,
    padding: 0,
    listStyleType: 'none',
  },
  divider: {
    height: theme.spacing.unit * 2,
  },
});

class Index extends React.Component {
  state = {
    single: '',
    suggestions: [],
    suggestionsData:[]
  };

  componentDidMount() {
    axios.get('https://api.iextrading.com/1.0/ref-data/symbols')
    .then(response =>  {
      if(response.status === 200) {
        this.setState({
        suggestionsData:response.data
        })
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  handleSuggestionsFetchRequested = ({ value }) => {
    let data = this.state.suggestionsData;
    this.setState({
      suggestions: getSuggestions(value,data),
    });
  };

  handleSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  };

  handleChange = name => (event, { newValue }) => {
    this.setState({
      [name]: newValue,
    });
  };
  handleSuggestionSelected = (event, { suggestion, suggestionValue }) => {

          this.props.history.push(`/search/${this.state.single}`)
      
  };


  render() {
    const { classes } = this.props;
    const autosuggestProps = {
      renderInputComponent,
      suggestions: this.state.suggestions,
      onSuggestionsFetchRequested: this.handleSuggestionsFetchRequested,
      onSuggestionsClearRequested: this.handleSuggestionsClearRequested,
      onSuggestionSelected:this.handleSuggestionSelected,
      getSuggestionValue,
      renderSuggestion,
    };
   
    return (
      <div  className={classes.root}>
        <Typography className={classes.head} variant="h4" gutterBottom>
          Material-UI Autosuggest 
        </Typography>
        <div  className={classes.auto}> 
        <Autosuggest
          {...autosuggestProps}
          inputProps={{
            classes,
            placeholder: 'input the stock symbol eg. MSFT',
            value: this.state.single,
            onChange: this.handleChange('single'),
          }}
          theme={{
            container: classes.container,
            suggestionsContainerOpen: classes.suggestionsContainerOpen,
            suggestionsList: classes.suggestionsList,
            suggestion: classes.suggestion,
          }}
          renderSuggestionsContainer={options => (
            <Paper {...options.containerProps} square>
              {options.children}
            </Paper>
          )}
        />
        </div>
        <div className={classes.divider} />
      </div>
    );
  }
}

Index.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Index);