import React from "react";
import Grid from '@material-ui/core/Grid';
import axios from 'axios';
import moment from 'moment';

const apiUrl = "https://api.iextrading.com/1.0/stock/";
const apiUr11 = "http://localhost:4000/api/";

class Search extends React.Component {
  state = {
    single: '',
    company:'',
    logo:'',
    news:'',
    summary:'',
    show:false,
  }

  componentDidMount() {

   let symbol = this.props.match.params.symbol ;

   axios.all([
       axios.get(`${apiUrl}${symbol}/company`),
       axios.get(`${apiUrl}${symbol}/logo`),
       axios.get(`${apiUrl}${symbol}/news`),
       axios.get(`${apiUr11}/stock/${symbol}`)
     ])
     .then(axios.spread((company, logo, news, summary) => {
       this.setState({ 
          show:true,
          company:company.data,
          logo:logo.data,
          news:news.data,
          summary:summary.data.data
        })
        }))
     .catch(error => console.log(error));
  }

 render() {
  const { show, company, news, summary, logo} = this.state;
  let newsData;

   if(show && news !== '') {
     newsData = news.map((data,i) => {
       return (
          <div key={i}>
            <p><b>Date:</b> {moment(data.datetime).format("LL") }</p>
            <p> <b>HeadLine: </b>{data.headline}</p>
          </div>
        );
      });
    }

  return (
       <Grid 
	       container
	       spacing={24}
	       direction="column"
         className="search search-wrapper"
	     >
        <Grid item xs={12} style={{padding:20}}>
           <h3 className="user-title">Search Result For - {this.props.match.params.symbol}  </h3>
        </Grid>
         <Grid item xs={12} style={{padding:20}}>
          {show ?
         <div>
           <div>
             <img src={logo.url} alt="logo" />
          </div>

          <p><b>Company Name : </b> {company.companyName}</p>
          
          <p><b>Industry :</b>  {summary.industry }</p>

          <p><b>Sector :</b>  {summary.sector}</p>
          <div>
            <p><b>Latest News for -</b>  {company.companyName}</p>
             {newsData}
          </div>
        </div>
        : <div>No Data Found for symbol </div>
        }
        </Grid>
      </Grid>
  );
  }
}

export default Search;
